<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        //TODO implement this!
         // Reads file as a string
        $file = file_get_contents($filePath);
        // converts all the string characters in lower case
        $file = strtolower($file);
        // removes unnecessary characters
        $file = preg_replace('/[^a-z]/i','', $file);
        // counts numbers of occurrences of every character
        return count_chars ($file,1);
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        //TODO implement this!
        
        // Sort the array
        asort($parsedFile);
        // counts number of characters
        $size = count($parsedFile);
        // find the median index (-1 at the end because of 0 based index of array)
        $medianIndex = floor(($size+1)/2)-1;

        // finds the $medianIndex entry in our Associative array
        $keys = array_keys($parsedFile);
        //single median letter
        $key = $keys[$medianIndex];
        //occurences of median  letter
        $value = $parsedFile[$key];
        $occurrences = $value;
        return chr($key);
    }
}