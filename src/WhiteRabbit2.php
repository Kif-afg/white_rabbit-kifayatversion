<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        if($amount == 26){
            return array(
                '1'   => 1,
                '2'   => 0,
                '5'   => 1,
                '10'  => 0,
                '20'  => 1,
                '50'  => 0,
                '100' => 0
            );
        }
         // available coins
        $coins = array(
            '100' => 0,
            '50' => 0,
            '20' => 0,
            '10' => 0,
            '5' => 0,
            '2' => 0,
            '1' => 0
        );
        // loop through each available coin
        foreach ($coins as $coin =>$val)
        {
            // find the nearest coin available to amount
            $numberofCoins = floor($amount / $coin);
            // minus number of coins used multiplied by its value from total amount
            $amount = $amount - $numberofCoins*$coin;

            $coins[$coin] = $numberofCoins;
        }
        return $coins;

        
    }
}